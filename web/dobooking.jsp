<%@page import="java.sql.*;"%>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false);
    
    Connection con=dbConn.myConn();

    String sn=(String)hs.getAttribute("uid");
    System.out.println(sn);      

    String fromdate=request.getParameter("fromdate");
    String todate=request.getParameter("todate");
    String message=request.getParameter("message");
    String vhid=request.getParameter("vhid");
	int status=0;
	
	PreparedStatement ps = con.prepareStatement("INSERT INTO tblbooking(userEmail,VehicleId,FromDate,ToDate,message,Status) VALUES(?,?,?,?,?,?)");
        ps.setString(1, sn);
        ps.setString(2, vhid);
		ps.setString(3, fromdate);
        ps.setString(4, todate);
		ps.setString(5, message);
        ps.setInt(6, status);
        int flag = ps.executeUpdate();  
		if(flag>0)
		{
		out.print("<script>alert('Booking successfull.');</script>");
                  response.sendRedirect("my-booking.jsp");  
		}
		else 
		{
		out.print("<script>alert('Something went wrong. Please try again');</script>");
		  response.sendRedirect("index.jsp");  
                }               
%>