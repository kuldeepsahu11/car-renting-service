<%@page import="java.sql.*;"%>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false);
 
    Connection con=dbConn.myConn();
   

    String sn=(String)hs.getAttribute("uid");
    System.out.println(sn);
    if(hs.getAttribute("uid")==null || hs.getAttribute("uid").equals(""))         
       {
       response.sendRedirect("index.jsp");  
       }    
   else{        
    String pagetype=request.getParameter("type");
    String pagedetails=request.getParameter("pagedetails");                               
 
%>	

<jsp:include page="includes/head.jsp" />
	<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
<script type="text/javascript" src="nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<body>
	<jsp:include page="includes/header.jsp" />
	<div class="ts-main-content">
	<jsp:include page="includes/leftbar.jsp" />
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">
					
						<h2 class="page-title">Manage Pages </h2>

						<div class="row">
							<div class="col-md-10">
								<div class="panel panel-default">
									<div class="panel-heading">Form fields</div>
									<div class="panel-body">
										<form method="post" name="chngpwd" class="form-horizontal" action="updatepage.jsp" onSubmit="return valid();">
										
											
  	        	 	<%//if (flag<=0) {%><div class="errorWrap"><strong>ERROR</strong>:<%//out.print("Something went wrong. Please try again");%> </div><%// } %>
                        <%//if (flag>0) {%>  <div class="succWrap"><strong>SUCCESS</strong>:<%//out.print("Testimonial Successfully (Active/Inactive)");%> </div>
                          <%// } %>
											<div class="form-group">
												<label class="col-sm-4 control-label">select Page</label>
												<div class="col-sm-8">
															   <select name="menu1" onChange="MM_jumpMenu('parent',this,0)">
                  <option value="" selected="selected" class="form-control">***Select One***</option>
                  <option value="manage-pages.jsp?type=terms">terms and condition</option>
                  <option value="manage-pages.jsp?type=privacy">privacy and policy</option>
                  <option value="manage-pages.jsp?type=aboutus">aboutus</option> 
                  <option value="manage-pages.jsp?type=faqs">FAQs</option>
                </select>
												</div>
											</div>
											<div class="hr-dashed"></div>
											
											<div class="form-group">
												<label class="col-sm-4 control-label">selected Page</label>
												<div class="col-sm-8">
						<%
			/*
			
				case "terms" :
									out.print("Terms and Conditions");
									break;
				
				case "privacy" :
									out.print("Privacy And Policy");
									break;
				
				case "aboutus" :
									out.print("About US");
									break;
			
				case "faqs" :
									out.print("FAQs");
									break;
											
				default :
								out.print("");
								break;
			if (pagetype.equals("terms"))
	{Terms and Conditions} else if (pagetype.equals("privacy")) {Privacy And Policy
		} else if (pagetype.equals("aboutus")) {About US	
			} else if (pagetype.equals("faqs")) {FAQs} else {}
			}*/ %>

	
												</div>
											</div>
								
									<div class="form-group">
												<label class="col-sm-4 control-label">Page Details </label>
												<div class="col-sm-8">
			<textarea class="form-control" rows="5" cols="50" name="pagedetails" id="pgedetails" placeholder="Package Details" required>
			<%  
        PreparedStatement ps1 = con.prepareStatement("SELECT detail from tblpages where type=?");
        ps1.setString(1, pagetype);       
        ResultSet rs1 = ps1.executeQuery();                         
        while(rs1.next())
        {             
            out.print(rs1.getString("detail"));
        }    
%>									

										</textarea> 
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-sm-8 col-sm-offset-4">
								
												<button type="submit" name="submit" value="Update" id="submit" class="btn-primary btn">Update</button>
												</div>
											</div>

										</form>

									</div>
								</div>
							</div>
							
						</div>
						
					

					</div>
				</div>
				
			
			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<jsp:include page="includes/script.jsp" />

</body>

</html>
<% } %>