<%@page import="java.sql.*;"%>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false);
    
        Connection con=dbConn.myConn();   

    String sn=(String)hs.getAttribute("uid");
    System.out.println(sn);
    if(hs.getAttribute("uid")==null || hs.getAttribute("uid").equals(""))         
       {
       response.sendRedirect("index.jsp");  
       }    
   else{                              
%>
<jsp:include page="includes/head.jsp" />

<body>
	<jsp:include page="includes/header.jsp" />

	<div class="ts-main-content">
		<jsp:include page="includes/leftbar.jsp" />
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">

						<h2 class="page-title">Registered Users</h2>

						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
							<div class="panel-heading">Reg Users</div>
							<div class="panel-body">
								<%//if (flag<=0) {%><div class="errorWrap"><strong>ERROR</strong>:<%//out.print("Something went wrong. Please try again");%> </div><%// } %>
                        <%//if (flag>0) {%>  <div class="succWrap"><strong>SUCCESS</strong>:<%//out.print("Testimonial Successfully (Active/Inactive)");%> </div>
                          <%// } %>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
										<th>#</th>
												<th> Name</th>
											<th>Email </th>
											<th>Contact no</th>
										<th>DOB</th>
										<th>Address</th>
										<th>City</th>
										<th>Country</th>
										<th>Reg Date</th>
										
										</tr>
									</thead>
									<tfoot>
										<tr>
										<th>#</th>
											<th> Name</th>
											<th>Email </th>
											<th>Contact no</th>
										<th>DOB</th>
										<th>Address</th>
										<th>City</th>
										<th>Country</th>
										<th>Reg Date</th>
										</tr>
										</tr>
									</tfoot>
									<tbody>

									<%  
        PreparedStatement ps = con.prepareStatement("SELECT * from  tblusers");
        ResultSet rs = ps.executeQuery();  
int i=1;                          
         while(rs.next())
                    {                     
%>	
										<tr>
											<td><%=i++%></td>
											<td><%= rs.getString("FullName") %></td>
											<td><%= rs.getString("EmailId") %></td>
											<td><%= rs.getString("ContactNo") %></td>
                                                                                        <td><%= rs.getString("dob") %></td>
											<td><%= rs.getString("Address") %></td>
											<td><%= rs.getString("City") %></td>
											<td><%= rs.getString("Country") %></td>
											<td><%= rs.getString("RegDate") %></td>
										</tr>
										<% } %>
										
									</tbody>
								</table>

						

							</div>
						</div>

					

					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<jsp:include page="includes/script.jsp" />
</body>
</html>
<% } %>
