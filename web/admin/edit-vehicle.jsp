<%@page import="java.sql.*;" %>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false); 
    
    Connection con=dbConn.myConn(); 

    String sn=(String)hs.getAttribute("uid");
    System.out.println(sn);
    if(hs.getAttribute("uid")==null || hs.getAttribute("uid").equals(""))         
       {
       response.sendRedirect("index.jsp");  
       }    
   else{               
%>
<jsp:include page="includes/head.jsp" />

<body>
	<jsp:include page="includes/header.jsp" />
	<div class="ts-main-content">
	<jsp:include page="includes/leftbar.jsp" />
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">
					
						<h2 class="page-title">Edit Vehicle</h2>

						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Basic Info</div>
									<div class="panel-body">
<%//if (flag<=0) {%><div class="errorWrap"><strong>ERROR</strong>:<%//out.print("Something went wrong. Please try again");%> </div><%// } %>
                        <%//if (flag>0) {%>  <div class="succWrap"><strong>SUCCESS</strong>:<%//out.print("Testimonial Successfully (Active/Inactive)");%> </div>
                          <%// } %>
				

                          <%   
                          String id=request.getParameter("id");
                          System.out.println(id); 
    PreparedStatement ps1 = con.prepareStatement("SELECT tblvehicles.*,tblbrands.BrandName,tblbrands.id as bid from tblvehicles join tblbrands on tblbrands.id=tblvehicles.VehiclesBrand where tblvehicles.id=?");
    ps1.setString(1, id);
    ResultSet rs1 = ps1.executeQuery();  
  System.out.println("hello");    
    while(rs1.next())
    {               System.out.println("hello");       
%>
<form method="post" class="form-horizontal" enctype="multipart/form-data" action="doeditvehicle.jsp">
    <div class="form-group">
    <label class="col-sm-2 control-label">Vehicle Title<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <input type="text" name="vehicletitle" class="form-control" value="<%= rs1.getString("VehiclesTitle") %>" required>
    </div>
    <label class="col-sm-2 control-label">Select Brand<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <select class="selectpicker" name="brandname" required>
    <option value="<%= rs1.getInt("bid") %>"><%= rs1.getString("BrandName") %> </option>
    </select>
    <input type="hidden" name="hid" value="<%= id %>">
    </div>
    </div>

    <div class="hr-dashed"></div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Vehical Overview<span style="color:red">*</span></label>
    <div class="col-sm-10">
    <textarea class="form-control" name="vehicaloverview" rows="3" required><%= rs1.getString("VehiclesOverview") %></textarea>
    </div>
    </div>

    <div class="form-group">
    <label class="col-sm-2 control-label">Price Per Day(in USD)<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <input type="text" name="priceperday" class="form-control" value="<%= rs1.getString("PricePerDay") %>" required>
    </div>
    <label class="col-sm-2 control-label">Select Fuel Type<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <select class="selectpicker" name="fueltype" required>
    <option value="<%= rs1.getString("FuelType") %>"> <%= rs1.getString("FuelType") %> </option>

    <option value="Petrol">Petrol</option>
    <option value="Diesel">Diesel</option>
    <option value="CNG">CNG</option>
    </select>
    </div>
    </div>


    <div class="form-group">
    <label class="col-sm-2 control-label">Model Year<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <input type="text" name="modelyear" class="form-control" value="<%= rs1.getString("ModelYear") %>" required>
    </div>
    <label class="col-sm-2 control-label">Seating Capacity<span style="color:red">*</span></label>
    <div class="col-sm-4">
    <input type="text" name="seatingcapacity" class="form-control" value="<%= rs1.getString("SeatingCapacity") %>" required>
    </div>
    </div>
    <div class="hr-dashed"></div>								
    <div class="form-group">
    <div class="col-sm-12">
    <h4><b>Vehicle Images</b></h4>
    </div>
    </div>


    <div class="form-group">
    <div class="col-sm-4">
    Image 1 <img src="img/vehicleimages/<%= rs1.getString("Vimage1") %>" width="300" height="200" style="border:solid 1px #000">
    <a href="changeimage1.jsp?imgid=<%= rs1.getInt("id") %>">Change Image 1</a>
    </div>
    <div class="col-sm-4">
    Image 2<img src="img/vehicleimages/<%= rs1.getString("Vimage2") %>" width="300" height="200" style="border:solid 1px #000">
    <a href="changeimage2.jsp?imgid=<%= rs1.getInt("id") %>">Change Image 2</a>
    </div>
    <div class="col-sm-4">
    Image 3<img src="img/vehicleimages/<%= rs1.getString("Vimage3") %>" width="300" height="200" style="border:solid 1px #000">
    <a href="changeimage3.jsp?imgid=<%= rs1.getInt("id") %>">Change Image 3</a>
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-4">
    Image 4<img src="img/vehicleimages/<%= rs1.getString("Vimage4") %>" width="300" height="200" style="border:solid 1px #000">
    <a href="changeimage4.jsp?imgid=<%= rs1.getInt("id") %>">Change Image 4</a>
    </div>
    <div class="col-sm-4">
    Image 5
    <% if(rs1.getString("Vimage5").equals(""))
    {
    out.print("File not available");
    } else { %>
    <img src="img/vehicleimages/<%= rs1.getString("Vimage5") %>" width="300" height="200" style="border:solid 1px #000">
    <a href="changeimage5.jsp?imgid=<%= rs1.getInt("id") %>">Change Image 5</a>
    <% } %>
    </div>

    </div>
    <div class="hr-dashed"></div>									
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
    <div class="panel-heading">Accessories</div>
    <div class="panel-body">
        
    <div class="form-group">
    <div class="col-sm-3">
    <% if(rs1.getString("AirConditioner").equals("1"))
    { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="airconditioner" checked value="1">
    <label for="inlineCheckbox1"> Air Conditioner </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="airconditioner" value="1">
    <label for="inlineCheckbox1"> Air Conditioner </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("PowerDoorLocks").equals("1"))
    { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powerdoorlocks" checked value="1">
    <label for="inlineCheckbox2"> Power Door Locks </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-success checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powerdoorlocks" value="1">
    <label for="inlineCheckbox2"> Power Door Locks </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("AntiLockBrakingSystem").equals("1"))
    { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="antilockbrakingsys" checked value="1">
    <label for="inlineCheckbox3"> AntiLock Braking System </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="antilockbrakingsys" value="1">
    <label for="inlineCheckbox3"> AntiLock Braking System </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("BrakeAssist").equals("1"))
    {
            %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="brakeassist" checked value="1">
    <label for="inlineCheckbox3"> Brake Assist </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="brakeassist" value="1">
    <label  for="inlineCheckbox3"> Brake Assist </label>
    </div>
    <% } %>
    </div>

    <div class="form-group">
    <% if(rs1.getString("PowerSteering").equals("1"))
    {
            %>
    <div class="col-sm-3">
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powersteering" checked value="1">
    <label for="inlineCheckbox1"> Power Steering </label>
    </div>
    <% } else { %>
    <div class="col-sm-3">
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powersteering" value="1">
    <label for="inlineCheckbox1"> Power Steering </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("DriverAirbag").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="driverairbag" checked value="1">
    <label for="inlineCheckbox2">Driver Airbag</label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="driverairbag" value="1">
    <label for="inlineCheckbox2">Driver Airbag</label>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("PassengerAirbag").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="passengerairbag" checked value="1">
    <label for="inlineCheckbox3"> Passenger Airbag </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="passengerairbag" value="1">
    <label for="inlineCheckbox3"> Passenger Airbag </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("PowerWindows").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powerwindow" checked value="1">
    <label for="inlineCheckbox3"> Power Windows </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="powerwindow" value="1">
    <label for="inlineCheckbox3"> Power Windows </label>
    </div>
    <% } %>
    </div>


    <div class="form-group">
    <div class="col-sm-3">
    <% if(rs1.getString("CDPlayer").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="cdplayer" checked value="1">
    <label for="inlineCheckbox1"> CD Player </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="cdplayer" value="1">
    <label for="inlineCheckbox1"> CD Player </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("CentralLocking").equals("1"))
    {
    %>
    <div class="checkbox  checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="centrallocking" checked value="1">
    <label for="inlineCheckbox2">Central Locking</label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-success checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="centrallocking" value="1">
    <label for="inlineCheckbox2">Central Locking</label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("CrashSensor").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="crashcensor" checked value="1">
    <label for="inlineCheckbox3"> Crash Sensor </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="crashcensor" value="1">
    <label for="inlineCheckbox3"> Crash Sensor </label>
    </div>
    <% } %>
    </div>
    <div class="col-sm-3">
    <% if(rs1.getString("LeatherSeats").equals("1"))
    {
    %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="leatherseats" checked value="1">
    <label for="inlineCheckbox3"> Leather Seats </label>
    </div>
    <% } else { %>
    <div class="checkbox checkbox-inline">
    <input type="checkbox" id="inlineCheckbox1" name="leatherseats" value="1">
    <label for="inlineCheckbox3"> Leather Seats </label>
    </div>
    <% } %>
    </div>
    </div>

<% } %> 


											<div class="form-group">
												<div class="col-sm-8 col-sm-offset-2" >
													
                                                                                                    <button class="btn btn-primary" name="submit" value="" type="submit" style="margin-top:4%">Save changes</button>
												</div>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>
						
					

					</div>
				</div>
				
			

			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<jsp:include page="includes/script.jsp" />
</body>
</html>
<% } %>