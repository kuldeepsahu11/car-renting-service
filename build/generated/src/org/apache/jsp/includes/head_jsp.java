package org.apache.jsp.includes;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class head_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
      out.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width,initial-scale=1\">\n");
      out.write("<meta name=\"keywords\" content=\"\">\n");
      out.write("<meta name=\"description\" content=\"\">\n");
      out.write("<title>CarAtRent</title>\n");
      out.write("<!--Bootstrap -->\n");
      out.write("<link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\" type=\"text/css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"assets/css/style.css\" type=\"text/css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"assets/css/owl.carousel.css\" type=\"text/css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"assets/css/owl.transitions.css\" type=\"text/css\">\n");
      out.write("<link href=\"assets/css/slick.css\" rel=\"stylesheet\">\n");
      out.write("<link href=\"assets/css/bootstrap-slider.min.css\" rel=\"stylesheet\">\n");
      out.write("<link href=\"assets/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("\t\t<link rel=\"stylesheet\" id=\"switcher-css\" type=\"text/css\" href=\"assets/switcher/css/switcher.css\" media=\"all\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/red.css\" title=\"red\" media=\"all\" data-default-color=\"true\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/orange.css\" title=\"orange\" media=\"all\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/blue.css\" title=\"blue\" media=\"all\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/pink.css\" title=\"pink\" media=\"all\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/green.css\" title=\"green\" media=\"all\" />\n");
      out.write("\t\t<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"assets/switcher/css/purple.css\" title=\"purple\" media=\"all\" />\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"assets/images/favicon-icon/apple-touch-icon-144-precomposed.png\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"assets/images/favicon-icon/apple-touch-icon-114-precomposed.html\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"assets/images/favicon-icon/apple-touch-icon-72-precomposed.png\">\n");
      out.write("<link rel=\"apple-touch-icon-precomposed\" href=\"assets/images/favicon-icon/apple-touch-icon-57-precomposed.png\">\n");
      out.write("<link rel=\"shortcut icon\" href=\"assets/images/favicon-icon/favicon.png\">\n");
      out.write("<link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,900\" rel=\"stylesheet\"> \n");
      out.write("<style>\n");
      out.write("\t\t.errorWrap {\n");
      out.write("    padding: 10px;\n");
      out.write("    margin: 0 0 20px 0;\n");
      out.write("    background: #fff;\n");
      out.write("    border-left: 4px solid #dd3d36;\n");
      out.write("    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);\n");
      out.write("    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);\n");
      out.write("}\n");
      out.write(".succWrap{\n");
      out.write("    padding: 10px;\n");
      out.write("    margin: 0 0 20px 0;\n");
      out.write("    background: #fff;\n");
      out.write("    border-left: 4px solid #5cb85c;\n");
      out.write("    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);\n");
      out.write("    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);\n");
      out.write("}\n");
      out.write("\t\t</style>\n");
      out.write("</head>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
