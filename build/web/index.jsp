<%@page import="java.sql.*"%>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false); 
    
    Connection con=dbConn.myConn();
%>
<% try { %>
<jsp:include page="includes/head.jsp"/>
<body>
        
<!--Header-->
<jsp:include page="includes/header.jsp"/>
<!-- /Header --> 

<!-- Banners -->
<section id="banner" class="banner-section">
  <div class="container">
    <div class="div_zindex">
      <div class="row">
        <div class="col-md-5 col-md-push-7">
          <div class="banner_content">
            <h1 style="color:darkorange">Find the right car for you.</h1>
            <p style="color:darkmagenta">We have more than a thousand cars for you to choose. </p>
            <a href="#" class="btn">Read More <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /Banners --> 


<!-- Resent Cat-->
<section class="section-padding gray-bg">
  <div class="container">
    <div class="section-header text-center">
      <h2>Find the Best <span>Car For You</span></h2>
      <p>There are various types of passenger cars and luxury cars are available for the users to hire at rent as their own choice at their own selected places. 
          And can make their tours and travels comfortable and more adventurous.
          There are multiple options provided for you under similar price range. And this can be done online in effective manner and book your car at any time and anywhere.</p>
    </div>
    <div class="row"> 
      
      <!-- Nav tabs -->
      <div class="recent-tab">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#resentnewcar" role="tab" data-toggle="tab">Cars  Available</a></li>
        </ul>
      </div>
      <!-- Recently Listed New Cars -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="resentnewcar">

<%
        PreparedStatement ps = con.prepareStatement("SELECT tblvehicles.VehiclesTitle,tblbrands.BrandName,tblvehicles.PricePerDay,tblvehicles.FuelType,tblvehicles.ModelYear,tblvehicles.id,tblvehicles.SeatingCapacity,tblvehicles.VehiclesOverview,tblvehicles.Vimage1 from tblvehicles join tblbrands on tblbrands.id=tblvehicles.VehiclesBrand");
        ResultSet rs = ps.executeQuery();                    
     while(rs.next())
      {                       
%>

<div class="col-list-3" >
<div class="recent-car-list">
<div class="car-info-box"> <a href="vehical-details.jsp?vhid=<%= rs.getInt("id") %>"><img src="admin/img/vehicleimages/<%= rs.getString("Vimage1") %>" class="img-responsive" alt="image"></a>
<ul>
<li><i class="fa fa-car" aria-hidden="true"></i><%= rs.getString("FuelType") %></li>
<li><i class="fa fa-calendar" aria-hidden="true"></i><%= rs.getString("ModelYear") %> Model</li>
<li><i class="fa fa-user" aria-hidden="true"></i><%= rs.getString("SeatingCapacity") %> seats</li>
</ul>
</div>
<div class="car-title-m">
<h6><a href="vehical-details.jsp?vhid=<%= rs.getInt("id") %>"><%= rs.getString("BrandName") %> , <%= rs.getString("VehiclesTitle") %></a></h6>
<span class="price"><img src="assets/images/rupees.jpg" height="20px" width="20px"><%= rs.getString("PricePerDay") %> /Day</span> 
</div>
<div class="inventory_info_m">
    <p><%= (rs.getString("VehiclesOverview")).substring(0, 100) %></p>
</div>
</div>
</div>
<% } %>
       
      </div>
    </div>
  </div>
</section>
<!-- /Resent Cat --> 

<!-- Fun Facts-->
<section class="fun-facts-section">
  <div class="container div_zindex">
    <div class="row">
      <div class="col-lg-3 col-xs-6 col-sm-3">
        <div class="fun-facts-m">
          <div class="cell">
            <h2><i class="fa fa-calendar" aria-hidden="true"></i>40+</h2>
            <p>Years Of Services</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6 col-sm-3">
        <div class="fun-facts-m">
          <div class="cell">
            <h2><i class="fa fa-user-circle-o" aria-hidden="true"></i>600+</h2>
            <p>Satisfied Customers</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6 col-sm-3">
        <div class="fun-facts-m">
          <div class="cell">
            <h2><i class="fa fa-car" aria-hidden="true"></i>100+</h2>
            <p>Brands Available</p>
          </div>
        </div>
      </div>      
      <div class="col-lg-3 col-xs-6 col-sm-3">
        <div class="fun-facts-m">
          <div class="cell">
            <h2><i class="fa fa-car" aria-hidden="true"></i>700+</h2>
            <p>New Cars For Rent</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Dark Overlay-->
  <div class="dark-overlay"></div>
</section>
<!-- /Fun Facts--> 


<!--Testimonial -->
<section class="section-padding testimonial-section parallex-bg">
  <div class="container div_zindex">
    <div class="section-header white-text text-center">
      <h2>Our Satisfied <span>Customers</span></h2>
    </div>
    <div class="row">
      <div id="testimonial-slider">
<%  
        String tid="1";
        PreparedStatement ps1 = con.prepareStatement("SELECT tbltestimonial.Testimonial,tblusers.FullName from tbltestimonial join tblusers on tbltestimonial.UserEmail=tblusers.EmailId where tbltestimonial.status=?");
        ps1.setString(1, tid);
        ResultSet rs1 = ps1.executeQuery();                    
     while(rs1.next())
                    {                               
%>


        <div class="testimonial-m">
          <div class="testimonial-img"> <img src="assets/images/cat-profile.png" alt="" /> </div>
          <div class="testimonial-content">
            <div class="testimonial-heading">
              <h5><%= rs1.getString("FullName") %></h5>
            <p><%= rs1.getString("Testimonial") %></p>
          </div>
        </div>
        </div>
        <% System.out.println(rs1.getString("Testimonial") ); } %>
        
       
  
      </div>
    </div>
  </div>
  <!-- Dark Overlay-->
  <div class="dark-overlay"></div>
</section>
<!-- /Testimonial--> 


<!--Footer -->
<jsp:include page="includes/footer.jsp"/>
<!-- /Footer--> 

<!--Back to top-->
<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
<!--/Back to top--> 

<!--Login-Form -->
<jsp:include page="includes/login.jsp"/>
<!--/Login-Form --> 

<!--Register-Form -->
<jsp:include page="includes/registration.jsp"/>

<!--/Register-Form --> 

<!--Forgot-password-Form -->
<jsp:include page="includes/forgotpassword.jsp"/>
<!--/Forgot-password-Form --> 

<!-- Scripts --> 
<jsp:include page="includes/script.jsp"/>

</body>
</html>


<% } catch(SQLException e)
{ out.print("Database not Connected or Entry not found");}
%>