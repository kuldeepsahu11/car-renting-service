<%@page import="java.sql.*;"%>
<%@page import="myConnect.dbConn;"%>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response.setDateHeader("Expires", 0); // Proxies.
    response.setContentType("text/html");
    
    HttpSession hs=request.getSession(false);
 
    Connection con=dbConn.myConn();
 

    String sn=(String)hs.getAttribute("uid");
    System.out.println(sn);
    if(hs.getAttribute("uid")==null || hs.getAttribute("uid").equals(""))         
       {
       response.sendRedirect("index.jsp");  
       }    
   else{ 
        
%>

<jsp:include page="includes/head.jsp" />
<body>
        
<!--Header-->
<jsp:include page="includes/header.jsp" />
<!--Page Header-->
<section class="page-header profile_page">
  <div class="container">
    <div class="page-header_wrap">
      <div class="page-heading">
        <h1>My Testimonials</h1>
      </div>
      <ul class="coustom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li>My Testimonials</li>
      </ul>
    </div>
  </div>
  <!-- Dark Overlay-->
  <div class="dark-overlay"></div>
</section>
<!-- /Page Header--> 

<%  
        PreparedStatement ps = con.prepareStatement("SELECT * from tblusers where EmailId=?");
        ps.setString(1,sn);
        ResultSet rs = ps.executeQuery();                           
        while(rs.next())
        {                     
%>

<section class="user_profile inner_pages">
  <div class="container">
    <div class="user_profile_info gray-bg padding_4x4_40">
      <div class="upload_user_logo"> <img src="assets/images/dealer-logo.jpg" alt="image">
      </div>

      <div class="dealer_info">
        <h5><%= rs.getString("FullName") %></h5>
        <p><%= rs.getString("Address") %><br>
          <%= rs.getString("City") %>&nbsp;<%= rs.getString("Country") %>
          <% } %></p>
      </div>
    </div>
  
  <div class="row">
      <div class="col-md-3 col-sm-3">
          <jsp:include page="includes/sidebar.jsp"/>
      <div class="col-md-8 col-sm-8">



        <div class="profile_wrap">
          <h5 class="uppercase underline">My Testimonials </h5>
          <div class="my_vehicles_list">
            <ul class="vehicle_listing">
                <%  
        PreparedStatement ps1 = con.prepareStatement("SELECT * from tbltestimonial where UserEmail=?");
        ps1.setString(1,sn);
        ResultSet rs1 = ps1.executeQuery();                           
        while(rs1.next())
        {                     
%>

              <li>
           
                <div>
                 <p><%= rs1.getString("Testimonial") %> </p>
                   <p><b>Posting Date:</b><%= rs1.getString("PostingDate") %> </p>
                </div>
                <% if(rs1.getInt("status")==1){ %>
                 <div class="vehicle_status"> <a class="btn outline btn-xs active-btn">Active</a>

                  <div class="clearfix"></div>
                  </div>
                  <% } else { %>
               <div class="vehicle_status"> <a href="#" class="btn outline btn-xs">Waiting for approval</a>
                  <div class="clearfix"></div>
                  </div>
                  <% } %>
              </li>
              <% } %>
              
            </ul>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/my-vehicles--> 

<<!--Footer -->
<jsp:include page="includes/footer.jsp"/>
<!-- /Footer--> 

<!--Back to top-->
<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>

<!-- Scripts --> 
<jsp:include page="includes/script.jsp"/>

</body>
</html>
   <% } %>